# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.
# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.bin" ] ; then
    PATH="$HOME/.bin:$PATH"
    PATH="$HOME/.bin/shortcuts/:$PATH"
fi

if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0192033" #black
    echo -en "\e]P8666666" #darkgrey

    echo -en "\e]P1A62A3E" #darkred
    echo -en "\e]P9F04758" #red

    echo -en "\e]P238912B" #darkgreen
    echo -en "\e]PA93C724" #green

    echo -en "\e]P3B27D12" #brown
    echo -en "\e]PBDDB62B" #yellow

    echo -en "\e]P4355C9A" #darkblue
    echo -en "\e]PC45A3E6" #blue

    echo -en "\e]P57C4F9F" #darkmagenta
    echo -en "\e]PDC953EF" #magenta

    echo -en "\e]P6258F8F" #darkcyan
    echo -en "\e]PE60C6C8" #cyan

    echo -en "\e]P777858C" #lightgrey
    echo -en "\e]PFC0C0C0" #white

    read -p "fbTerm? " -n 1 -r
    echo
    if [[ $REPLY =~ ^[YyДд]$ ]]
    then
        FBTERM=1 exec fbterm
    fi
fi
