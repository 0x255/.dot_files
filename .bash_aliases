# vim: set filetype=sh

## красим вывод
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

if [ -x /usr/bin/grc ]; then
    alias diff="grc --colour=auto diff"
    alias netstat="grc --colour=auto netstat"
    alias ping="grc --colour=auto ping"
    alias gcc="grc --colour=auto gcc"
    alias traceroute="grc --colour=auto traceroute"
    alias last="grc --colour=auto last"
    alias mount=" sudo grc mount"
    alias ifconfig="sudo grc ifconfig"
    alias ps="grc ps"
    alias tail="grc tail"
    alias tail="grc head"
    alias tail="grc cat"
    alias make="grc make"
fi


## BASH config
alias bash.update=". ~/.bashrc"
alias bash.rc="vim ~/.bashrc"
alias bash.aliases="vim ~/.bash_aliases && bash.update"
alias bash.functions="vim ~/.bash_functions && bash.update"
alias bash.hotkeys="bind -p | grep -v '^#\|self-insert\|^$'"
alias bash.history="vim ~/.bash_history"


## MOVE shortcuts
alias ~='cd ~'
alias ..='cd ..'
alias ...='cd ../../'
alias cd..='cd ..'
alias cd.prev='cd -'


### ALIASES

# Basics overrides
alias cp='cp -iv'
alias cpf='rsync --progress'
#     cpssh /from user@host:/to
alias cpssh='rsync --progress -avz -e ssh'

alias mv='mv -iv'
alias rm='rm -iv'
alias ls='ls -1Flh --color --group-directories-first'
alias la='ls -a'
alias df='df -Th'
alias du='du -h'

alias dmesg='dmesg -T'
alias wget='wget -c'
alias tmux='tmux attach || tmux new -s "tmux"'
alias ssh.passwd='ssh -o PubkeyAuthentication=no'
alias ntpdate=' sudo ntpdate-debian'
alias cal='ncal -3'
alias ncal='ncal -3'

alias udiff='diff -ubBd'
alias ack.pager='ack --pager less'
alias pwd.clip='pwd | xclip'

alias s="sudo";
alias x="xdg-open"
alias service=" sudo service"
alias systemctl=" sudo systemctl"
alias systemctl.user="systemctl --user"
alias journalctl=" sudo journalctl"
alias halt=" sudo shutdown -h now"
alias reboot=" sudo shutdown -r now"

#APTitude
alias install=" sudo aptitude install"
alias reinstall=" sudo aptitude reinstall"
alias update=" sudo aptitude update"
alias full-upgrade=" sudo aptitude full-upgrade"
alias safe-upgrade=" sudo aptitude safe-upgrade"
alias show-upgrades=' aptitude search ~U -F"%c%M %p# %15v# %15V# %d"'
alias cleanup=' sudo apt-get autoclean && sudo apt-get autoremove && sudo apt-get clean && sudo apt-get remove'
alias show='aptitude show'
alias versions='aptitude versions'

# MOUNT
alias mount.iso=" sudo mount -t iso9660 -o loop"
alias mount.udf=" sudo mount -t udf -o loop"
alias mount.mdf=" sudo mount -o loop"
alias mount.nrg=" sudo mount -o loop,offset=307200"
alias mount.mdx=" sudo mount -o loop,offset=64"
alias mount.nandroid=" sudo mount -t ext3 -o loop"
alias umount=" sudo umount"

# EDITING
alias sv=" sudo vim"
alias sg=" gksudo gvim"

alias v="vim"
alias g="gvim"

alias vim.rc="vim ~/.vimrc"
alias vim.localrc="vim ~/.vimrc_local"
alias vim.essential=" vim -u ~/.vim/_evimrc "
alias vim.helptags="vim -c 'call plug#helptags()|q'"

alias vim.hosts=" sudo vim /etc/hosts"

## UTILS_SHORTCUTS
alias shell.color.test='msgcat --color=test'
alias shell.update='xrdb -load  ~/.Xdefaults'
alias monitoroff='xset dpms force off'
alias ape2flac='shnconv -o flac'

# поправить русскоязычные именя файлов и каталогов после разархивирования виндовых архивов
alias extract.fix='convmv -f cp1252 -t cp850 * --notest  && convmv -f cp866 -t utf-8 * --notest'

## LANG
alias св="cd"
alias ьс="mc"
alias ды="ls"
alias ап="fg"

## SHIT
alias :q='read -s -n1 -p "Do you realy want to quit the shell? [y]|n "; if [ "$REPLY" = y -o "$REPLY" = Y -o "$REPLY" = " " -o "$REPLY" = "" ]; then exit; else echo; unset REPLY; fi'
alias :q!='exit'

# Выставляем права на файл, без chmod:
# g+w <file>, o-x <file>, a+x <file>, ...
for PERM in 'r' 'w' 'x'
do
    for TARGET in 'u' 'g' 'o' 'a'
    do
        alias "$TARGET+$PERM"="chmod $TARGET+$PERM"
        alias "$TARGET-$PERM"="chmod $TARGET-$PERM"
    done
done

## GIT (from ~/.gitconfig.base)
alias gst='git st'
alias ga='git add'
alias gb='git b'
alias gba='git ba'
alias gd='git d'
alias gdc='git dc'
alias gc='git c'
alias gca='git gca'
alias gr='git r'
alias gri='git ri'
alias gl='git l'
