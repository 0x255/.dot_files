# vim: set filetype=sh

start() {  
    sudo service $1 start
}
stop() {  
    sudo service $1 stop    
}
restart() {  
    sudo service $1 restart 
}
status() {  
    sudo service $1 status  
}

# создать каталог и перейти в оный
mkcd() { mkdir -p "$@" && cd "$@"; }


## Распаковка архивов
#  example: extract file
extract () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.xz) tar xvfJ $1 ;;
            *.tar.bz2) tar xjf $1 ;;
            *.tar.gz) tar xzf $1 ;;
            *.bz2) bunzip2 $1 ;;
            *.rar) unrar x $1 ;;
            *.gz) gunzip $1 ;;
            *.tar) tar xf $1 ;;
            *.tbz2) tar xjf $1 ;;
            *.tbz) tar -xjvf $1 ;;
            *.tgz) tar xzf $1 ;;
            *.zip) unzip $1 ;;
            *.Z) uncompress $1 ;;
            *.7z) 7z x $1 ;;
            *) echo "I don't know how to extract '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

## Запаковать архив
#  example: pk tar file
pk () {
    if [ $1 ] ; then
        case $1 in
            tbz) tar cjvf $2.tar.bz2 $2 ;;
            tgz) tar --user-compress-program=pigz -czvf $2.tar.gz $2 ;;
            txz) tar --create --xz --file $2.tar.xz $2 ;;
            tar) tar cpvf $2.tar  $2 ;;
            bz2) bzip $2 ;;
            gz) gzip -c -9 -n $2 > $2.gz ;;
            zip) zip -r $2.zip $2 ;;
            7z) 7z a $2.7z $2 ;;
            *) echo "'$1' cannot be packed via pk()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}  

## Простой калькулятор
calc() {
    echo | awk "{print $@}"
}

## Если без аргументов, то su
function sudo() {
    command="$@"
    if [ -z "$command" ]; then
        command sudo su -
    else
        command sudo "$@"
    fi
}

## Построить дерево папок от текущей директории и ниже
function tree() {
    { [ ${#} = 0 ] && find . -type d | sed -e 1d -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|-/'; }
    { [ ${#} != 0 ] && find ${@} -type d | sed -e 1d -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|-/'; }
}
function tree_files() {
    { [ ${#} = 0 ] && find . | sed -e 1d -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|-/'; }
    { [ ${#} != 0 ] && find ${@} | sed -e 1d -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|-/'; }
}

## top 10 команд
function top10() {
    echo "top 10 commands:"
    history | awk '{print $2}' | awk 'BEGIN {FS="|"}{print $1}' | sort | uniq -c | sort -nr | head
}

## заставочка уничтожающия CPU))))
function koffie() { 
    cat /dev/urandom | hexdump -C | grep --color=auto "ca fe";
}

## история по пакетам
function apt-history() {
    case "$1" in
        install)
            cat /var/log/dpkg.log | grep 'install '
        ;;
        upgrade|remove)
            cat /var/log/dpkg.log | grep $1
        ;;
        rollback)
            cat /var/log/dpkg.log | grep upgrade | \
            grep "$2" -A10000000 | \
            grep "$3" -B10000000 | \
            awk '{print $4"="$5}'
        ;;
        *)
            cat /var/log/dpkg.log
        ;;
    esac
}

## получаемм недостающие ключики для репозиториев
function apt-autokey() {
    sudo apt-get update 2> /tmp/keymissing

    for key in "$( grep "NO_PUBKEY" /tmp/keymissing |sed 's/.*NO_PUBKEY //' )"
    do
        echo -e "\nProcessing key: $key"
        gpg --keyserver pool.sks-keyservers.net --recv $key && gpg --export --armor $key | sudo apt-key add -
    done
}

# выставляет права $2 ТОЛЬКО на папки $1
function chmod.dir() {
    find $2 -type d -exec chmod $1 '{}' \;    
}
function chmod.file() {
    find $2 -type f -exec chmod $1 '{}' \;    
}

function myip() {
    wget -qO - http://checkip.dyndns.org/ | awk '{ print $6 }' | sed "s/<\/.*$//g"
}

# аналог watch, но красящий вывод
zrrepeat() {
    # $1 - комманда bash
    # $2 - время в формате sleep (default: 5s)
    # $3 - флаг очистки экрана (default: true)

    if [ -z "$2" ]; then
        delay='5s'
    else
        delay="$2"
    fi

    export TZ="MSK"
    start_time=$(date +%X)
    start_timestamp=$(date +%s)
    count=0

    while true; do
        if [ -z "$3" ]; then
            /usr/bin/clear
        fi
        
        ((count++))
        current_time=$(date +%X);
        current_timestamp=$(date +%s)
        
        elapsed_timestamp=$(($current_timestamp-$start_timestamp))
        elapsed_time=$(date -d@$elapsed_timestamp +%X);

        text_count='count '
        text_current_time='time '
        text_start_time='started '
        text_elapsed_time='elapsed '
        text_delay='delay '

        echo -e "\n\E[01;31m>> \E[01;30m$text_count\E[01;31m$count\E[01;30m/$text_current_time\E[01;31m$current_time\E[01;30m/$text_elapsed_time\E[01;31m$elapsed_time \E[01;30m[$text_start_time$start_time; $text_delay$delay]\E[033;00m"
        echo "`eval $1`"

        sleep "$delay"
    done;
}

# Установка переменной http_proxy. Варианты - on, off, свой вариант
sethttpproxy() {
    case "${1}" in
    on)
        export http_proxy='192.168.0.252:3128' ;;
    off)
        export http_proxy='' ;;
    *)
        export http_proxy="${1}" ;;
    esac
}
