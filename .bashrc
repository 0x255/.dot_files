export DEBEMAIL=0x255@sonictorment.ru
export DEBFULLNAME='0x255'

# Если у пользователя есть каталог ~/bin - добавляем его в PATH
# Частный случай, т.к. конфиг root - симлинк на этот файл
if [ -d /home/0x255/.bin ]; then
  export PATH="/home/0x255/.bin:$PATH"
  export PATH="/home/0x255/.bin/shortcuts:$PATH"
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# LOCAL
if [ -f ~/.bashrc_local ]; then
    . ~/.bashrc_local
fi
if [ -f ~/.bash_aliases_local ]; then
    . ~/.bash_aliases_local
fi
if [ -f ~/.bash_functions_local ]; then
    . ~/.bash_functions_local
fi
if [ -d $HOME/.bash_completion.d ] && ! shopt -oq posix; then
    for file in $HOME/.bash_completion.d/*; do
        if [ -f $file ]; then
            . $file
        fi
    done
fi

# если не интерактивный сеанс, то выходим 
[ -z "$PS1" ] && return

export HISTFILE=~/.bash_history
export HISTSIZE=30000
# добавлять историю комманд, а не перезаписывать оную
shopt -s histappend
PROMPT_COMMAND='history -a;history -n'
# в хистори не будут писаться команды &, ls, bg, fg, exit.  
# Можно  дописать  и  свои,  через  двоеточие, можно использовать шаблоны.
export HISTIGNORE="&:ls:[bf]g:mc:df:du:df:bc:exit:halt:shutdown:reboot"
# не заносить в хистори повторяющиеся друг за другом и начинающиеся с пробела команды
HISTCONTROL=ignoreboth:eraseups
# Ведение лога истории с datestamp'ом:
#export HISTTIMEFORMAT="%t%d.%m.%y %H:%M:%S%t"

### Опции шела
# http://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html

# проверять размер окна после каждой комманды, обновлять LINES и COLUMNS при необходимости
shopt -s checkwinsize
# добавления возможности ввода многострочных команд
shopt -s cmdhist
# эвристическое исправление ошибок для cd
shopt -s cdspell
# переход в папку просто по указанию её имени (без явно заданной команды cd)
shopt -s autocd
# при указании $VAR_PATH делаем переход по пути, содержащимуся в переменной
shopt -s cdable_vars
# использовать регистронезависимые маски 
#   например ls *.jpg выведет и *.jpg и *.JPG
shopt -s nocaseglob
# реккурсивный glob **
#   например rm -r ./**/__pycache__/ удалит все каталоги __pycache__ (с содержимым) от текущего
shopt -s globstar

export EDITOR='vim'
export VISUAL='vim'
export VIEWER='view'
export PAGER="less"

# настраиваем less
export LESS="-isMR"

# красим less и man
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m']

# делаем вывод less более дружелюбным для non-text файлов
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

### отключаем ^s и ^q
[[ $- =~ i ]] && stty -ixoff -ixon

# навигация в стиле vi
# http://www.catonmat.net/download/bash-vi-editing-mode-cheat-sheet.pdf
set -o vi

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python2
source /usr/local/bin/virtualenvwrapper.sh

if [ -f ~/.bash_colors ]; then
    . ~/.bash_colors
fi

# $$    - PID запущеннго шела,
# $PPID - parent PID
if [ "$UID" != '0' ]; then
    COLOR=$IRed
else
    COLOR=$IYellow
fi 
# PS1="["'$(date +%g%m%d\ %H%M%S)'"] \u@\h \w\n \$ "
export PS1="${COLOR}["'$(date +%g%m%d\ %H%M%S)'"]${IBlack} $$ ${COLOR}\u@\h \w\n \$${Reset} "


#clear

# делаем цвета в tty как в терминале
# if [ "$TERM" = "linux" ] || [ "$TERM" = "fbterm" ]; then
#     COLOR_FILE="$( cat /home/0x255/.Xdefaults | egrep -v '^(!|$)' | grep '".colors/' | sed 's/.*"\([^"]*\)".*/\1/' )"
#     cat $HOME/$COLOR_FILE | egrep -v '^(!|$)' | egrep '^*color'| while read line; do
#         COLOR_NUM=$(echo $line | sed 's/.*color\([^"]*\):.*/\1/')
#         COLOR_NUM=$( printf "%X" $COLOR_NUM )
#         COLOR_HEX=$(echo $line | awk '{print $2}'| cut -b2- )
#     done
#     #clear
# fi

# ускоряется старт gtk программ и исчезает куча warning/error в консоли 
export NO_AT_BRIDGE=1 

case "$TERM" in
    rxvt*)
        if [ -e /usr/share/terminfo/r/rxvt-unicode-256color ]; then
            export TERM=xterm-256color
        elif [ -e /usr/share/terminfo/r/rxvt-256color ]; then
            export TERM=xterm-256color
        elif [ -e /usr/share/terminfo/r/rxvt-color ]; then
            export TERM=xterm-256color
        else
            export TERM=xterm
        fi
        ;;
    xterm*)
        if [ -e /usr/share/terminfo/x/xterm-256color ]; then
            export TERM=xterm-256color
        elif [ -e /usr/share/terminfo/x/xterm-color ]; then
            export TERM=xterm-256color
        else
            export TERM=xterm
        fi
        ;;
    linux)
        [ -n "$FBTERM" ] && export TERM=fbterm
        ;;
esac

alias sudo='sudo '
